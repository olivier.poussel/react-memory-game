import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Memory from './pages/memory';
import './styles/app.css';

/**
 * Point d'entrée de l'application,
 * ainsi que le routeur qui sert à naviguer entre les pages de l'application
 */
export default function App() {
  return (
    <HashRouter>
      <Switch>
        <Route path='/' component={Memory} />
      </Switch>
    </HashRouter>
  )
}